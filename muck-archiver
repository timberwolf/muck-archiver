#!/usr/bin/expect -f
#                                muck-archiver                                #
#                     Created by Timberwolf on 2020-10-01                     #
#                                                                             #
#   This is free and unencumbered software released into the public domain.   #
#                     See COPYING.md for more information.                    #
#__                                                                         __#

if {$argc < 4} {
	send_user "Usage: muck-archiver <server> <port> <char> <dumpfile>\n"
	exit
}

set server   [lindex $argv 0]
set port     [lindex $argv 1]
set char     [lindex $argv 2]
set dumpfile [lindex $argv 3]

set timeout 10

spawn openssl s_client -quiet -connect $server -port $port

# connected properly
# the welcome blurb will probably mention WHO in it somewhere
expect -timeout 30 "WHO"
# gobble up input for 1 second to finish spitting out the welcome blurb
expect -timeout 1 "foobarbaz"

# ask for password
while {true} {
	stty -echo
	send_user "Password: "
	expect_user -timeout 60 -re "(.*)\n"
	send_user "\n"
	set password $expect_out(1,string)
	stty echo
	
	send "connect $char $password\r"
	
	expect -timeout 2 "Either that player does not exist, or has a different password." continue
	# it didn't match the failed-password text
	break
}

send_user "\n-- starting dump --\n\n"

send "@archive me\r"
expect "\\\[Start Dump\\\]"
log_file -noappend $dumpfile
send_log "\[Start Dump\]\r\r\n"
expect -timeout 120 "\\\[End Dump\\\]"
log_file

send "QUIT"
