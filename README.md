# MUCK Archiver

Dump your MUCK characters to a file!

## Dependencies

muck-archiver requires the OpenSSL command-line tool (to poke the server) and expect.

## Installation

Make the muck-archiver script executable (although it should be already), and drop it in your `PATH`.

## Usage

```
muck-archiver <server> <port> <character> <dump file>
```

You will be prompted for your character password during login.

It's probably a good idea to make sure you're not logged in with your regular client, as Fuzzball MUCKs will spam the dump to your regular client too!

----------------------------------------

_This script is free and unencumbered software released into the public domain; see [COPYING.md](COPYING.md) for more information._
